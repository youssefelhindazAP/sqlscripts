USE ModernWays;
SELECT MIN(GemiddeldeCijfer) AS Gemiddelde FROM 
(SELECT AVG(Cijfer) AS GemiddeldeCijfer
FROM Evaluaties
GROUP BY Studenten_Id) AS GemiddeldeCijfers;