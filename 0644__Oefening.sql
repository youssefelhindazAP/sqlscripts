USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumRelease`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumRelease`()
BEGIN
	DECLARE numberOfAlbums, numberOfBands, randomAlbumId, randomBandId INT DEFAULT 0;
    
    SELECT COUNT(*) INTO numberOfAlbums FROM Albums;
	SELECT COUNT(*) INTO numberOfBands FROM Bands;
    
    SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
    SET randomBandId = FLOOR(RAND() * numberOfBands) + 1;
    
    IF (randomAlbumId, randomBandId) NOT IN (SELECT * FROM Albumreleases) THEN
		INSERT INTO Albumreleases(Bands_Id, Albums_Id)
        VALUES
        (randomBandId, randomAlbumId);
	END IF;
END$$

DELIMITER ;