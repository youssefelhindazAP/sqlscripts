/*Ideale prefixlengte voor Voornaam*/
SELECT COUNT(DISTINCT LEFT(Voornaam, 9))
FROM Muzikanten;
/*Nu checken voor Familienaam*/
SELECT COUNT(DISTINCT LEFT(Familienaam, 9))
FROM Muzikanten;
/*Index creëren*/
CREATE INDEX VoornaamFamilienaamidx
ON Muzikanten(Voornaam(9), Familienaam(9));