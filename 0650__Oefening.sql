USE `aptunes`;
DROP procedure IF EXISTS `DangerousInsertAlbumreleases`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DangerousInsertAlbumreleases` ()
BEGIN
	DECLARE random INT DEFAULT FLOOR(RAND() * 3) + 1;
	DECLARE teller INT DEFAULT(0);
	DECLARE numberOfAlbums, numberOfBands, randomAlbumId, randomBandId INT DEFAULT(0);
	DECLARE totalInAlbumReleases INT DEFAULT(SELECT COUNT(*) FROM AlbumReleases);
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
		SELECT 'Nieuwe releases konden niet worden toegevoegd.' Message;
	END;
	   
	SELECT COUNT(*) FROM Albums INTO numberOfAlbums;
	SELECT COUNT(*) FROM Bands INTO numberOfBands;

	START TRANSACTION;
		REPEAT
			SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
			SET randomBandId = FLOOR(RAND() * numberOfBands) + 1;
			INSERT INTO AlbumReleases(Bands_Id, Albums_Id)
			VALUES (randomBandId, randomAlbumId);
			-- Indien niets is toegevoegd
			IF((SELECT COUNT(*) FROM AlbumReleases) < totalInAlbumReleases) THEN
				SIGNAL SQLSTATE '45001';
			END IF;
			SET teller = teller + 1;
			-- vanaf de tweede invoeging een kans van 1 op 3 SIGNAL
			IF teller = 2 THEN 
				IF random = 1 THEN SIGNAL SQLSTATE '45000'; 
				END IF;
			END IF;
		UNTIL teller >= 3
		END REPEAT;
	COMMIT;
END$$

DELIMITER ;