USE `aptunes`;
DROP procedure IF EXISTS `GetLiedjes`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetLiedjes`(IN songName VARCHAR(50))
BEGIN
	SELECT Titel
	FROM Liedjes
	WHERE Titel LIKE CONCAT('%', songName, '%');
END$$

DELIMITER ;