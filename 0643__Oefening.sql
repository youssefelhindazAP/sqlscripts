USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateAndReleaseAlbum`(IN titel VARCHAR(100), IN bands_Id INT)
BEGIN
	START TRANSACTION;
	INSERT INTO Albums (Titel) VALUES (titel);
    INSERT INTO Albumreleases (Bands_id, Albums_id) VALUES(bands_Id, LAST_INSERT_ID());
    COMMIT;
END$$

DELIMITER ;