USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `NumberOfGenres` (OUT total TINYINT)
BEGIN
	SELECT COUNT(*)
    INTO total
    FROM Genres;
END$$

DELIMITER ;