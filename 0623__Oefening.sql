-- toont alle geldige combinaties van liedjestitels en genres
SELECT Titel, Naam
FROM Liedjesgenres INNER JOIN Liedjes
ON Liedjesgenres.Liedjes_Id = Liedjes.Id
INNER JOIN Genres
ON Liedjesgenres.Genres_Id = Genres.Id;

/*Sinds er bij Genres "full Table Scan" staat, vermoed ik
dat ik hiervoor een gewone index moet aanmaken voor Naam?*/
CREATE INDEX NaamIdx
ON Genres(Naam);