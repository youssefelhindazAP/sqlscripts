-- toont per naam het aantal keer dat iemand met die naam lid is van een groep
-- iemand die lid is van twee groepen, wordt dus twee keer geteld
-- naamgenoten zijn mogelijk en worden samen geteld
SELECT Voornaam, Familienaam, count(Lidmaatschappen.Muzikanten_Id)
FROM Muzikanten INNER JOIN Lidmaatschappen
ON Lidmaatschappen.Muzikanten_Id = Muzikanten.Id
GROUP BY Familienaam, Voornaam
ORDER BY Voornaam, Familienaam;

/*GROUP BY komt eerst, dus neem ik ze op de volgorde van hoe ze worden vermeld. In dit geval
dus eerst Familienaam en dan Voornaam*/
CREATE INDEX FamilienaamVoornaamIdx
ON Muzikanten(Familienaam, Voornaam);