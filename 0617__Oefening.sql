USE ModernWays;
CREATE VIEW AuteursBoekenRatings
AS
SELECT AuteursBoeken.Auteur, AuteursBoeken.Titel, GemiddeldeRatings.Rating FROM AuteursBoeken
INNER JOIN GemiddeldeRatings ON GemiddeldeRatings.Boeken_Id = AuteursBoeken.Boeken_Id;

SELECT * FROM AuteursBoekenRatings;