CREATE USER IF NOT EXISTS student@localhost 
IDENTIFIED BY 'ikbeneenstudent';

GRANT EXECUTE ON PROCEDURE aptunes.GetAlbumDuration TO student@localhost;
GRANT EXECUTE ON PROCEDURE aptunes.GetAlbumDuration2 TO student@localhost;
GRANT SELECT ON TABLE aptunes.Liedjes TO student@localhost;