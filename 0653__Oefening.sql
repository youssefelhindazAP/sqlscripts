USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration2`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration2` (IN getAlbumId INT, 
OUT totalDuration SMALLINT UNSIGNED)
SQL SECURITY INVOKER
BEGIN
	DECLARE songDuration TINYINT UNSIGNED;
	DECLARE ok INTEGER DEFAULT 0;
    
	DECLARE currentLength
	CURSOR FOR SELECT Lengte FROM Liedjes WHERE Albums_Id = getAlbumId;

	DECLARE CONTINUE HANDLER
	FOR NOT FOUND SET ok = 1;
    
    SET totalDuration = 0;
    
    OPEN currentLength;
		getTotalLength: LOOP
			FETCH currentLength INTO songDuration;
			IF ok = 1 THEN
				LEAVE getTotalLength;
			END IF;
			SET totalDuration = totalDuration + songDuration;
        END LOOP getTotalLength;
    CLOSE currentLength;
END$$

DELIMITER ;