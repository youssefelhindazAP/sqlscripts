USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleaseWithSuccess`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleaseWithSuccess` (OUT success BOOL)
BEGIN
	DECLARE numberOfAlbums, numberOfBands, randomAlbumId, randomBandId INT DEFAULT 0;
    
    SELECT COUNT(*) INTO numberOfAlbums FROM Albums;
	SELECT COUNT(*) INTO numberOfBands FROM Bands;
    
    SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
    SET randomBandId = FLOOR(RAND() * numberOfBands) + 1;
    
    IF (randomAlbumId, randomBandId) NOT IN (SELECT * FROM Albumreleases) THEN
		INSERT INTO Albumreleases(Bands_Id, Albums_Id)
        VALUES
        (randomBandId, randomAlbumId);
        SET success = 1;
	ELSE
		SET success = 0;
	END IF;
END$$

DELIMITER ;