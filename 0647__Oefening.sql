USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleasesLoop` (IN extraReleases INT)
BEGIN
	DECLARE counter INT DEFAULT 0;
    DECLARE success BOOL;
    
    callLoop: LOOP
    CALL MockAlbumReleaseWithSuccess(success);
    IF succes = 1 THEN
		SET counter = counter + 1;
	END IF;
    IF counter = extraReleases THEN
		LEAVE callLoop;
	END IF;
    END LOOP;
END$$

DELIMITER ;