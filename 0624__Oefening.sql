-- toont combinaties van liedjes en bands
-- doet dit enkel voor liedjestitels die beginnen met 'A'
-- gaat van kort naar lang
SELECT Titel, Naam, Lengte FROM Liedjes
INNER JOIN Bands
ON Liedjes.Bands_Id = Bands.Id
WHERE Titel LIKE 'A%'
ORDER BY Lengte;

/*Sinds LIKE eerst aan bod komt, neem ik Titel en maak hiervan een index*/
CREATE INDEX TitelIdx
ON Liedjes(Titel);