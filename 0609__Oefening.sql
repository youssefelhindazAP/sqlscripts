USE ModernWays;
SELECT Games.Titel, Platformen.Naam
FROM Releases INNER JOIN Platformen ON Platformen.Id = Releases.Platformen_Id
RIGHT JOIN Games ON Games.Id = Releases.Games_Id;