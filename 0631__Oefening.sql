USE ModernWays;
SELECT DISTINCT Voornaam FROM Studenten
-- Geeft dezelfde resultaat als ik '= ANY' verplaats met IN
WHERE (Voornaam = ANY(SELECT Voornaam FROM Personeelsleden)
AND Voornaam = ANY(SELECT Voornaam FROM Directieleden));