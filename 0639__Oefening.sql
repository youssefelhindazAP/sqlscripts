USE ModernWays;
INSERT INTO Boeken (
   Titel,
   Stad,
   Verschijningsdatum,
   Commentaar,
   Categorie,
   Personen_Id
)
VALUES (
   'De Woorden',
   'Antwerpen',
   '1962',
   'Een zeer mooi boek.',
   'Roman',
   (SELECT Id FROM Personen WHERE
       Familienaam = 'Sartre' AND Voornaam = 'Jean-Paul'))