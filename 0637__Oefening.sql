USE ModernWays;
-- STAP 1: De tabel Personen creëren op basis van de tabel Boeken
-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- gegevens uit de tabel Boeken overzetten naar de tabel Personen
-- we gebruiken hiervoor een subquery
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;
   
-- Verifiëren dat de dubbele eruit zijn gehaald
select Voornaam, Familienaam from Personen
    order by Voornaam, Familienaam;
    
-- STAP 2: Overige kolommen aan de tabel Personen toevoegen
-- Andere kolommen toevoegen in Personen incl. de primar key
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);

-- We testen om te zien als we hebben wat we wilden, 
-- namelijk een tabel met de voornamen en familienamen van de auteurs:
select * from Personen order by Familienaam, Voornaam;

-- STAP 3: De tabel Boeken en Personen linken
-- We hebben nu beide tabellen, maar nog geen relatie om beide te linken met elkaar
-- We beginnen met een nieuwe foreign key voor in de tabel Boeken:
-- note: ik krijg geen foutmelding als ik de not null code probeer te doen, dus doe ik het direct met null:
alter table Boeken add Personen_Id int null;

-- STAP 4: De foreign key kolom in de tabel Boeken invullen
-- Om de Id van Personen te kopiëren in de tabel Boeken, 
-- moeten we eerst een relatie leggen tussen de twee tabellen
-- de kolom Voornaam en Familienaam zijn gelijk in beide tabellen
-- dus linken we op basis van deze kolommen als volgt:
select Boeken.Voornaam,
   Boeken.Familienaam,
   Boeken.Personen_Id,
   Personen.Voornaam,
   Personen.Familienaam,
   Personen.Id
from Boeken cross join Personen
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;

-- We kunnen dezelfde redenering gebruiken in een update. 
-- Om de waarde van Id van de Personen tabel te kopiëren naar de Personen_Id kolom van Boeken volstaat het dan om in de where clausule van de update instructie de voorwaarde mee te geven 
-- dat de waarden beide kolommen aan elkaar gelijk moeten zijn.
SET SQL_SAFE_UPDATES = 0;
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
SET SQL_SAFE_UPDATES = 1;

-- Nu kunnen we de not null constraint aan de kolom Personen_Id in de tabel Boeken terug toevoegen:
-- foreign key verplicht maken
alter table Boeken change Personen_Id Personen_Id int not null;

-- Testen of dat alles komt wat we willen
select Voornaam, Familienaam, Personen_Id from Boeken;

-- STAP 5: Dubbele kolommen verwijderen uit de tabel Boeken
alter table Boeken drop column Voornaam,
    drop column Familienaam;

-- STAP 6: De foreign key constraint op de kolom Personen_Id toevoegen
-- Nu kunnen we de kolom Personen_Id "promoveren" tot een echte foreign key kolom
-- Dus nu ook de constraint toevoegen:
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);
-- Je ziet vanonder een tabel met Voornaam, familienaam en personen_Id.
-- Dat komt door de laatste select die werd gegeven in de opgave.
-- Beide tabellen zijn dus in orde.