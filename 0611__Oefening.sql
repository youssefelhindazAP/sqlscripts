USE ModernWays;
SELECT games.Titel, COALESCE( releases.Games_Id, 'geen platformen gekend' ) AS Platformen
FROM games LEFT JOIN releases ON Id = Games_Id
WHERE Games_Id IS NULL
UNION
SELECT COALESCE(releases.Platformen_Id, 'geen games gekend') , platformen.Naam
FROM releases RIGHT JOIN platformen ON Platformen_Id = Id
WHERE Platformen_Id IS NULL;