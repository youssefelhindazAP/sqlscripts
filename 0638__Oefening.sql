USE ModernWays;
-- Inserten van de persoonsgegevens
INSERT INTO Personen (
   Voornaam, Familienaam, AanspreekTitel
)
VALUES (
   'Hilary', 'Mantel', 'Mevrouw'
);

-- Eerst haar primary key zoeken en vervolgens het boek toevoegen:
INSERT INTO Boeken (
   Titel, Stad, Uitgeverij, Verschijningsdatum,
   Herdruk, Commentaar, Categorie, Personen_Id
)
VALUES (
   'Wolf Hall', '', 'Fourth Estate; First Picador Edition First Printing edition',
   '2010', '', 'Goed boek', 'Thriller', 11
);